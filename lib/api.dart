import 'dart:async';
import 'dart:developer';
import 'dart:html' as html;
import 'dart:io';

import 'data/bout/Bout.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class API {
  String authority;
  String graphqlUrl;
  String graphqlKey;


  late final graphqlHost = graphqlUrl.replaceAll("https://", "").replaceAll("/graphql", "");

  API({
    this.authority = 'uot31q76k2.execute-api.us-east-1.amazonaws.com',
    this.graphqlUrl = 'https://7b4574yugjc6hai7asb7ow7fyy.appsync-api.us-east-1.amazonaws.com/graphql',
    this.graphqlKey = 'da2-ourzdchyovhrrjwyd3kkyelr6y'
  }) {
    final wsLink = graphqlUrl.replaceAll("https", "wss").replaceAll("appsync-api", "appsync-realtime-api");
    final headers = {
      "host": graphqlUrl.replaceAll("https://", "").replaceAll("/graphql", ""),
      "x-api-key": graphqlKey
    };
    final headersB64 = base64.encode(utf8.encode(jsonEncode(headers)));

    final connectionURL = "$wsLink?header=$headersB64&payload=e30=";

    ws = html.WebSocket(
      connectionURL,
      [
        "graphql-ws"
      ],
    );

    ws.onOpen.listen((event) {
      print(event.type);
    });

    ws.onError.listen((event) {
      print(event.type);
    });

    ws.onMessage.asBroadcastStream().listen((event) {
      if (event.type == "ka") {
        lastWsAck = DateTime.now().millisecondsSinceEpoch;
      }
    });

    ws.onClose.listen((event) {
      print(event);
    });

  }

  int? lastWsAck;
  late html.WebSocket ws;

  Future<String?> upload(Bout bout) {
    Map<String, dynamic> boutJSON = bout.toJson();
    final resp = http.put(
      Uri.https(authority, 'bout'),
      headers: { 'Access-Control-Allow-Origin': '*' },
      body: json.encode(boutJSON)
    );
    Future<String?> id = resp.then((response) {
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return null;
      }
    });
    return id;
  }

  Future<Bout?> get(String id) {
    final resp = http.get(
        Uri.https(authority, 'bout'),
        headers: { 'id': id, 'Access-Control-Allow-Origin': '*' }
    );
    Future<Bout?> bout = resp.then((response) {
      if (response.statusCode == 200) {
        String body = response.body;
        Map<String, dynamic> boutJSON = json.decode(body);
        Bout bout = Bout.fromJson(boutJSON);
        return bout;
      } else {
        return null;
      }
    });
    return bout;
  }

  Stream<Bout> watchBout(String id) {
    final StreamController<Bout> streamController = StreamController();
    final subscriptionDocument = (
      """subscription MySubscription(\$hash:String) {
  onUpdateBout(hash:\$hash) {
    hash
  time
  cards {
    items {
      P
      fencer
      realtime
      time
      type
    }
  }
  format {
    freefencing
    nonCom
    overtimeLength
    periods
    timePerBreak
    timePerPeriod
    touchesToWin
  }
  metadata {
    description
    id
    lastUpdated
    left {
      id
      name
    }
    name
    right {
      id
      name
    }
    started
  }
  noncoms {
    items {
      fencer
      realtime
      time
    }
  }
  phases {
    items {
      end
      length
      realTimeStart
      start
      type
    }
  }
  priority
  score {
    touches {
      realtime
      scorer
      time
    }
  }
  winner
  }
}"""
    ).replaceAll("\n", "").replaceAll(RegExp(" +"), " ");

    final subscription = {
      "query": subscriptionDocument,
      "variables": {
        "hash": id
      }
    };

    final register = {
      'id': id,
      'payload': {
        'data': jsonEncode(subscription),
        'extensions': {
          'authorization': {
            'host':graphqlHost,
            'x-api-key':graphqlKey
          }
        }
      },
      'type': 'start'
    };

    void send() {
      switch(ws.readyState) {
        case 0:
          Future.delayed(const Duration(seconds: 1), send);
          break;
        case 1:
          ws.send(jsonEncode(register));
          break;
        default:
          break;
      }
    }
    send();


    ws.onMessage.asBroadcastStream().listen((event) {
      final json = jsonDecode(event.data);
      if (json["id"] == id) {
        if (json["type"] == "data") {
          Map<String,dynamic> boutJson = json["payload"]["data"]["onUpdateBout"];
          Bout bout = Bout.fromJson(boutJson);
          streamController.add(bout);
        }
      }
    });

    return streamController.stream.asBroadcastStream();
  }


}