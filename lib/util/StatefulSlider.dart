// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:refflow/util/DigitField.dart';

import '../generated/l10n.dart';
import 'enum.dart';

class StatefulSlider extends StatefulWidget {
  const StatefulSlider({
    Key? key,
    this.min = 0,
    this.max = 1,
    this.value = 0.5,
    this.interval,
    this.description,
    this.type = SliderType.num,
    this.display,
    required this.onChanged,
  }) : super(key: key);

  final double min;
  final double max;
  final double value;
  final double? interval;
  final String? description;
  final SliderType type;
  final Widget Function(double)? display;
  final Function(double) onChanged;

  @override
  State<StatefulSlider> createState() => _SliderState();
}

class _SliderState extends State<StatefulSlider> {
  late double _value = widget.value;

  void setValue(value) {
    if (value > widget.max) {
      value = widget.max;
    }
    if (value < widget.min) {
      value = widget.min;
    }
    setState(() { _value = value; });
    widget.onChanged(value);
  }

  int? get divisions {
    if (widget.interval == null) {
      return null;
    } else {
      return (widget.max - widget.min) ~/ widget.interval!;
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      Expanded(
        child: Slider(
          min: widget.min,
          max: widget.max,
          value: _value,
          onChanged: (value) {
            double adj = (value * 1000).ceilToDouble() / 1000;
            setValue(adj);
          },
          divisions: divisions,
        ),
      )
    ];
    if (widget.display != null) {
      Widget display = widget.display!(_value);
      children.add(
        InkWell(
          onTap: () => showDialog(
              context: context,
              builder: (context) {
                Widget child;
                switch(widget.type) {
                  case SliderType.time:
                    child = Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(widget.description ?? S.of(context).manualEntry),
                          ConstrainedBox(
                            constraints: const BoxConstraints(minHeight: 50),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                    width: 50,
                                    child: DigitField(
                                        (widget.min / 60).floorToDouble(),
                                        (widget.max / 60).floorToDouble(),
                                        (_value / 60).floorToDouble(),
                                        null,
                                        (value) {
                                          double seconds = _value % 60;
                                          setValue(value * 60 + seconds);
                                        }
                                    )
                                ),
                                const Text(":"),
                                SizedBox(
                                    width: 50,
                                    child: DigitField(
                                        0,
                                        59,
                                        _value % 60,
                                        2,
                                        (value) {
                                          double minutes = (_value / 60).floorToDouble();
                                          setValue(minutes * 60 + value);
                                        }
                                    )
                                ),

                              ],
                            ),
                          )
                        ],
                      ),
                    );
                    break;
                  case SliderType.num:
                    child = Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(widget.description ?? S.of(context).manualEntry),
                          ConstrainedBox(
                            constraints: const BoxConstraints(minHeight: 50),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                    width: 100,
                                    child: DigitField(widget.min, widget.max, _value, null, (value) {
                                      setValue(value);
                                    })
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                    break;
                }
                return Dialog(
                  child: child
                );
              }
          ),
          child: display,
        )
      );
    }
    return Row(
      children: children
    );
  }
}