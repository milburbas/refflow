enum Fencer {
  left, right
}

enum CardType {
  yellow, red, black
}

enum PhaseType {
  period, break_, overtime, intermission
}

enum DeltaType {
  touch, start
}

enum SliderType {
  time, num
}