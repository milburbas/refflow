

extension DoubleExt on double {
  double roundTo(int decimalPlaces, {bool ceil = false}) {
    int scalar = 10^decimalPlaces;
    if (ceil) {
      return (this*scalar).ceilToDouble() / scalar;
    } else {
      return (this*scalar).roundToDouble() / scalar;
    }

  }
}