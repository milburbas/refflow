import 'dart:math';

import 'package:refflow/util/doubles.dart';

String secDoubleToString (double secDouble, bool? dropCentiseconds) {
  int places;
  if (dropCentiseconds ?? false) {
    places = 0;
  } else {
    places = 2;
  }
  double snappedSecDouble = secDouble.roundTo(places, ceil: true);

  double flooredSecDouble = max(0, snappedSecDouble);



  int minutes = (flooredSecDouble / 60).floor();
  int seconds = (flooredSecDouble - minutes * 60).floor();
  int centiseconds = ((flooredSecDouble - minutes * 60 - seconds) * 100).floor();

  if (centiseconds == 100) {
    seconds++;
    centiseconds = 0;
  }

  if (seconds == 60) {
    minutes++;
    seconds = 0;
  }

  String paddedMinutes = minutes.toString();
  String paddedSeconds = seconds.toString().length < 2 ? '0$seconds' : seconds.toString();
  String paddedCentiseconds = centiseconds.toString().length < 2 ? '0$centiseconds' : centiseconds.toString();

  return dropCentiseconds ?? false ? '$paddedMinutes:$paddedSeconds' : '$paddedMinutes:$paddedSeconds.$paddedCentiseconds';
}