extension ListExt<E> on List<E> {
  E? maxByOrNull(Comparable? Function(E a) get) {
    if (length == 0) return null;
    E? current;
    for (var item in this) {
      var value = get(item);
      if (value != null && current != null) {
        if (value.compareTo(get(current)) > 0) current = item;
      } else if (value != null) {
        current ??= item;
      }
    }
    return current;
  }

  E? minByOrNull(Comparable? Function(E a) get) {
    if (length == 0) return null;
    E? current;
    for (var item in this) {
      var value = get(item);
      if (value != null && current != null) {
        if (value.compareTo(get(current)) < 0) current = item;
      } else if (value != null) {
        current ??= item;
      }
    }
    return current;
  }

  int count(bool Function(E a) check) {
    int result = 0;
    for (var item in this) {
      if (check(item)) {
        result++;
      }
    }
    return result;
  }

  num sumOf(num Function(E a) get) {
    num result = 0;
    for (var item in this) {
      result += get(item);
    }
    return result;
  }

  E? firstWhereOrNull(bool Function(E a) test) {
    if (length == 0) return null;
    for (var item in this) {
      if (test(item)) {
        return item;
      }
    }
    return null;
  }

  E? lastWhereOrNull(bool Function(E a) test) {
    if (length == 0) return null;
    for (var item in reversed) {
      if (test(item)) {
        return item;
      }
    }
    return null;
  }

  List<List<E>> chunk(int size) {
    List<List<E>> subsets = List.empty(growable: true);
    for (E item in this) {
      int index = indexOf(item);
      if (index % size == 0) {
        subsets.add(List.empty(growable: true));
      }
      subsets.last.add(item);
    }
    return subsets;
  }
}