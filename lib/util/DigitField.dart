// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: non_constant_identifier_names
Widget DigitField(
    double min,
    double max,
    double value,
    int? pad,
    Function(double) onComplete,
    ) {
  TextEditingController txt = TextEditingController();
  txt.text = value.toString();
  FocusNode focus = FocusNode();
  void validate(bool complete) {
    if (txt.text == null || txt.text.isEmpty) {
      txt.text = min.toString();
    }
    if (int.parse(txt.text) > max) {
      txt.text = max.toString();
    }
    if (int.parse(txt.text) < min) {
      txt.text = min.toString();
    }
    if (txt.text.length < (pad ?? double.negativeInfinity)) {
      txt.text = "0" * (pad! - txt.text.length) + txt.text;
    }
    if (complete) onComplete(double.parse(txt.text));
  }
  validate(false);
  focus.addListener(() => validate(true));
  return TextField(
    focusNode: focus,
    controller: txt,
    autocorrect: false,
    textAlign: TextAlign.center,
    onEditingComplete: () {
      validate(true);
    },
    onSubmitted: (value) => validate(true),
    inputFormatters: [
      TextInputFormatter.withFunction((oldValue, newValue) {
        if (newValue.text.contains(RegExp(r'\D'))) {
          return oldValue;
        }

        return newValue;
      })
    ],
  );
}