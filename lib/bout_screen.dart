import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:refflow/data/bout/NonComs.dart';
import 'package:refflow/new_bout_screen.dart';
import 'package:refflow/util/StatefulSlider.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/lists.dart';
import 'package:refflow/util/sec_double_to_string.dart';

import 'bout/info_pane.dart';
import 'bout/scoring_pane.dart';
import 'data/bout/Bout.dart';
import 'data/bout/model.dart';
import 'generated/l10n.dart';

class BoutScreenArguments {
  final String boutString;

  BoutScreenArguments(this.boutString);

}

class BoutScreen extends StatelessWidget {

  late final Bout? bout;

  BoutScreen({Key? key, this.bout}) : super(key: key) {
    bout ??= defaultBout;
  }


  @override
  Widget build(BuildContext context) {
    // Wakelock.enable();
    Widget child = Scaffold(
        backgroundColor: Colors.black,
        body: Consumer<BoutModel>(
          builder: (context, boutModel, child) {
            List<String> messages = assembleMessages(boutModel);
            bool showHelp = boutModel.showHelp;
            if (showHelp) messages.add(S.of(context).moreOptions);
            return Stack(
                children: [
                  Column(
                    children: [
                      infoPane(
                          boutModel.reset,
                          boutModel.reverse,
                              () {
                            double length = 30;
                            showDialog(context: context, builder: (context) {
                              return AlertDialog(
                                title: Text(S.of(context).startIntermission),
                                content: SizedBox(
                                  width: 720,
                                  height: 50,
                                  child: StatefulSlider(
                                      min: 10,
                                      max: 180,
                                      value: length,
                                      interval: 5,
                                      type: SliderType.time,
                                      // description: "Length of the intermission.",
                                      display: (value) => SizedBox(
                                          width: 100,
                                          height: 50,
                                          child: Center(
                                              child: Text(
                                                  secDoubleToString(value, true),
                                                  textScaleFactor: 1.4
                                              )
                                          )
                                      ),
                                      onChanged: (value) {
                                        length = value;
                                      }
                                  ),
                                ),
                                actions: [
                                  ElevatedButton(onPressed: () {
                                    Navigator.of(context, rootNavigator: true).pop(null);
                                  }, child: const Text("Cancel")),
                                  ElevatedButton(onPressed: () {
                                    Navigator.of(context, rootNavigator: true).pop(true);
                                    boutModel.startIntermission(length);
                                  }, child: const Text("Confirm"))
                                ],
                              );
                            });
                          },
                              () {
                            if (boutModel.bout.priority == null) {
                              boutModel.assignPriority();
                            } else {
                              boutModel.clearPriority();
                              Future.delayed(const Duration(seconds: 1), boutModel.assignPriority);
                            }
                          },
                          boutModel.endPhase,
                              () => boutModel.undoTouch(null, force: true),
                          boutModel.clearPriority,
                              () {
                            boutModel.stop();
                            Future.microtask(() => Navigator.pushNamed(context, '/new', arguments: NewBoutScreenArguments(boutString: boutModel.bout.format.toString())));
                          },
                          boutModel.toFreefencing,
                          () {
                            Clipboard.setData(ClipboardData(
                              text: 'Time checks: ${boutModel.timerChecks}\nTime check time: ${boutModel.timerCheckTime}\nTime checks avg time: ${boutModel.timerCheckTime / boutModel.timerChecks}\nPhase checks: ${boutModel.phaseChecks}\nPhase check time: ${boutModel.phaseCheckTime}\nPhase checks avg time: ${boutModel.phaseCheckTime / boutModel.phaseChecks}\nScore checks: ${boutModel.scoreChecks}\nScore check time: ${boutModel.scoreCheckTime}\nScore checks avg time: ${boutModel.scoreCheckTime / boutModel.scoreChecks}'
                            ));
                            log('Time checks: ${boutModel.timerChecks}');
                            log('Time check time: ${boutModel.timerCheckTime}');
                            log('Time checks avg time: ${boutModel.timerCheckTime / boutModel.timerChecks}');
                            log('Phase checks: ${boutModel.phaseChecks}');
                            log('Phase check time: ${boutModel.phaseCheckTime}');
                            log('Phase checks avg time: ${boutModel.phaseCheckTime / boutModel.phaseChecks}');
                            log('Score checks: ${boutModel.scoreChecks}');
                            log('Score check time: ${boutModel.scoreCheckTime}');
                            log('Score checks avg time: ${boutModel.scoreCheckTime / boutModel.scoreChecks}');
                          },
                          (fencer, type) {
                            boutModel.stop();
                            CardType evaluatedCardType;
                            if (boutModel.bout.winner == null) {
                              evaluatedCardType = boutModel.card(fencer, type);
                            } else {
                              evaluatedCardType = type;
                            }

                            Color color;
                            switch(evaluatedCardType) {
                              case CardType.yellow:
                                color = const Color.fromARGB(255, 255, 255, 0);
                                break;
                              case CardType.red:
                                color = const Color.fromARGB(255, 255, 0, 0);
                                break;
                              case CardType.black:
                                color = const Color.fromARGB(255, 0, 0, 0);
                                break;
                            }
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return GestureDetector(
                                      onTap: () => Navigator.of(context, rootNavigator: true).pop(null),
                                      child: Container(
                                          decoration: BoxDecoration(color: color),
                                          height: double.infinity,
                                          width: double.infinity
                                      )
                                  );
                                }
                            );
                          },
                          boutModel.bout.metadata.id,
                          boutModel.bout.format.touchesToWin,
                          boutModel.bout.phases.period,
                          boutModel.bout.format.periods,
                          boutModel.time.time,
                          boutModel.delta,
                          boutModel.deltaType,
                          boutModel.timer.isActive(),
                          boutModel.bout.score.of(Fencer.left),
                          boutModel.bout.score.of(Fencer.right),
                          boutModel.bout.metadata.fencer(Fencer.left).name,
                          boutModel.bout.metadata.fencer(Fencer.right).name,
                          boutModel.bout.priority,
                          boutModel.bout.winner,
                          boutModel.bout.cards.items,
                          boutModel.isReversed,
                          messages
                      ),
                      scorePane(
                              (Fencer? fencer) {
                            boutModel.touch(fencer, silent: false);
                          },
                          boutModel.undoTouch,
                              () {
                            boutModel.toggle(silent: false);
                          },
                          boutModel.bout.winner == null,
                          true,
                          boutModel.isReversed,
                          showHelp
                      )
                    ],
                  ),

                  boutModel.showHelp ? GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    excludeFromSemantics: true,
                    onTapUp: (details) async {
                      boutModel.showHelp = false;
                      GestureBinding?.instance.handlePointerEvent(PointerDownEvent(
                        pointer: 999,
                        position: details.globalPosition,
                      ));
                      await Future.delayed(const Duration(milliseconds: 50));
                      GestureBinding?.instance.handlePointerEvent(PointerUpEvent(
                        pointer: 999,
                        position: details.globalPosition,
                      ));
                    },
                  ) : const IgnorePointer()
                ]
            );
          },
        )
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: const Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
    BoutModel boutModel = BoutModel(
        bout: bout!
    );
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => boutModel,
        ),
        ChangeNotifierProvider(
          create: (context) => boutModel.time,
        ),
        ChangeNotifierProvider(
          create: (context) => boutModel.time.minutes,
        ),
        ChangeNotifierProvider(
          create: (context) => boutModel.time.seconds,
        ),
        ChangeNotifierProvider(
          create: (context) => boutModel.time.centiseconds,
        )
      ],
      child: child,
    );
  }
}

List<String> assembleMessages(BoutModel boutModel) {
  List<String> messages = [];
  switch(boutModel.bout.winner) {
    case Fencer.left:
      messages.add(S.current.winMessage("left"));
      break;
    case Fencer.right:
      messages.add(S.current.winMessage("right"));
      break;
    default:
      break;
  }

  BoutNoncom? lastNonCom = boutModel.bout.noncoms.items.maxByOrNull((a) => a.time);

  if (lastNonCom != null) {
    if (boutModel.bout.time - lastNonCom.time < 10) {
      messages.add(S.current.noncombativity);
    }
  }

  switch(boutModel.currentPhase.type) {
    case PhaseType.period:
      break;
    case PhaseType.break_:
      messages.add(S.current.breakMessage);
      break;
    case PhaseType.overtime:
      messages.add(S.current.overtime);
      break;
    case PhaseType.intermission:
      messages.add(S.current.intermission);
      break;
  }
  switch(boutModel.bout.priority) {
    case Fencer.left:
      messages.add(S.current.priorityLeft);
      break;
    case Fencer.right:
      messages.add(S.current.priorityRight);
      break;
    default:
      break;
  }
  return messages;
}
