/// Flutter icons Refflow
/// Copyright (C) 2022 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  Refflow
///      fonts:
///       - asset: fonts/RefflowIcons.ttf
///
/// 
/// * Material Design Icons, Copyright (C) Google, Inc
///         Author:    Google
///         License:   Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
///         Homepage:  https://design.google.com/icons/
///
// ignore_for_file: constant_identifier_names

import 'package:flutter/widgets.dart';

class RefflowIcons {
  RefflowIcons._();

  static const _kFontFam = 'RefflowIcons';
  static const String? _kFontPkg = null;

  static const IconData stars = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData rotate_left = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_back = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_downward = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_forward = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_upward = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData compare_arrows = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData watch_later = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData circle = IconData(0xf111, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData menu = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
