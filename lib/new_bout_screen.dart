import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:refflow/refflow_icons.dart';
import 'package:refflow/util/StatefulSlider.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/sec_double_to_string.dart';

import 'bout_screen.dart';
import 'data/bout/Bout.dart';
import 'data/bout/Format.dart';
import 'generated/l10n.dart';

class NewBoutScreenArguments {
  final String? boutString;
  late String? formatString;

  NewBoutScreenArguments({this.boutString, this.formatString}) {
    if (boutString != null) {
      formatString ??= BoutFormat.fromJson(json.decode(boutString!)).toString();
    }
  }

}

class NewBoutScreen extends StatelessWidget {
  late final BoutFormat? boutFormat;
  NewBoutScreen({Key? key, this.boutFormat}) : super(key: key) {
    boutFormat ??= defaultBout.format;
  }



  @override
  Widget build(BuildContext context) {
    BoutFormat format = boutFormat!;
    return Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).newBout),
          leading: IconButton(onPressed: () => Navigator.pop(context), icon: const Icon(RefflowIcons.arrow_back)),
        ),
        backgroundColor: Colors.black,
        body: Column(
            children: [
              ParameterSlider(
                  title: S.of(context).periodLength,
                  min: 10,
                  max: 300,
                  value: format.timePerPeriod,
                  interval: 10,
                  type: SliderType.time,
                  onChanged: (value) => format.timePerPeriod = value
              ),
              ParameterSlider(
                  title: S.of(context).overtimeLength,
                  min: 10,
                  max: 180,
                  value: format.overtimeLength,
                  interval: 10,
                  type: SliderType.time,
                  onChanged: (value) => format.overtimeLength = value
              ),
              ParameterSlider(
                  title: S.of(context).touchesToWin,
                  min: 1,
                  max: 30,
                  value: format.touchesToWin.toDouble(),
                  interval: 1,
                  type: SliderType.num,
                  onChanged: (value) => format.touchesToWin = value.toInt()
              ),
              ParameterSlider(
                  title: S.of(context).periods,
                  min: 1,
                  max: 5,
                  value: format.periods.toDouble(),
                  interval: 1,
                  type: SliderType.num,
                  onChanged: (value) => format.periods = value.toInt()
              ),
              ParameterSlider(
                  title: S.of(context).timePerBreak,
                  min: 10,
                  max: 180,
                  value: format.timePerBreak.toDouble(),
                  interval: 10,
                  type: SliderType.time,
                  onChanged: (value) => format.timePerBreak = value
              ),
            ]
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Bout newBout = defaultBout;
            newBout.format = format;
            Navigator.pushNamed(context, '/bout', arguments: BoutScreenArguments(newBout.toString()));
          },
          tooltip: S.of(context).startAction,
          child: const Icon(RefflowIcons.arrow_forward),
        )
    );
  }
}

// ignore: non_constant_identifier_names
Widget ParameterSlider(
{
  required String title,
  String? description,
  double min = 0,
  double max = 1,
  double value = 0.5,
  double? interval,
  SliderType type = SliderType.num,
  required Function(double) onChanged
}
    ) {
  String getDisplayString(value) {
    switch(type) {
      case SliderType.time:
        return secDoubleToString(value, true);
      case SliderType.num:
        return value.toString();
    }
  }
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 8.0, top: 8.0),
        child: Text(title, textScaleFactor: 1.4),
      ),
      StatefulSlider(
        min: min,
        max: max,
        value: value,
        interval: interval,
        description: description,
        type: type,
        display: (value) => SizedBox(
            width: 100,
            height: 50,
            child: Center(
                child: Text(
                    getDisplayString(value),
                    textScaleFactor: 1.4
                )
            )
        ),
        onChanged: onChanged,
      )
    ],
  );
}