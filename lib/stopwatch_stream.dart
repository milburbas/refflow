import 'dart:async';

import 'dart:math';


class StopwatchStream {
  bool _active = false;
  Duration interval = const Duration(milliseconds: 10);
  double time = 0;

  final StreamController<double> _streamController = StreamController();


  double lastStopTime = 0.0;
  void stopTimer() {
    lastStopTime = time;
    _active = false;
  }

  double lastStartTime = 0.0;
  void startTimer() {
    lastStartTime = time;
    _active = true;
  }

  void toggleTimer() {
    if (_active) {
      stopTimer();
    } else {
      startTimer();
    }
  }

  bool isActive() {
    return _active;
  }

  StopwatchStream() {
    double lastTime = max(DateTime.now().millisecondsSinceEpoch / 1000, lastStartTime);
    void tick() {
      double newTime = DateTime.now().millisecondsSinceEpoch / 1000;
      if (_active) {
        double delta = newTime - lastTime;
        lastTime = newTime;
        time += delta;
        _streamController.add(delta);
        Timer(interval, tick);
      } else {
        lastTime = newTime;
        Timer(interval, tick);
      }
    }
    tick();
  }


  late Stream<double> stream = _streamController.stream.asBroadcastStream();
}