// ignore_for_file: file_names
import 'dart:convert';
import 'dart:math';

import 'package:refflow/util/doubles.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/lists.dart';

class BoutPhase {

  int realTimeStart = 0;
  double? length;
  double? end;
  double start = 0;
  PhaseType type = PhaseType.period;

  BoutPhase({
    required this.realTimeStart,
    this.length,
    this.end,
    required this.start,
    required this.type,
  });
  BoutPhase.fromJson(Map<String, dynamic> json) {
    realTimeStart = json['realTimeStart']?.toDouble();
    length = json['length']?.toDouble();
    end = json['end']?.toDouble();
    start = json['start']?.toDouble();
    type = PhaseType.values.firstWhere((element) => json['type']?.toString().endsWith(element.name) ?? false, orElse: () => PhaseType.period);
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['realTimeStart'] = realTimeStart;
    data['length'] = length?.roundTo(3);
    data['end'] = end?.roundTo(3);
    data['start'] = start.roundTo(3);
    data['type'] = type.toString();
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}

class BoutPhases {

  List<BoutPhase> items = List.empty();

  BoutPhases({
    required this.items,
  });
  BoutPhases.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      final v = json['items'];
      final arr0 = <BoutPhase>[];
      v.forEach((v) {
        arr0.add(BoutPhase.fromJson(v));
      });
      items = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    final v = items;
    final arr0 = [];
    for (var v in v) {
      arr0.add(v.toJson());
    }
    data['items'] = arr0;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }

  int get period => items.count((item) => item.type == PhaseType.period);

  double escapedLength(double start, double end, double realStart) {

    double nestedTime = items.sumOf((item) {
      if(item.start >= start && item.start < end && item.realTimeStart > realStart) {
        return escapedLength(item.start, min(item.end ?? double.infinity, end), item.realTimeStart.toDouble());
      } else {
        return 0.0;
      }
    }).toDouble();
    return end - start - nestedTime;
  }

  BoutPhase? get latest => items.maxByOrNull((a) {
    if (a.end == null) {
      return a.realTimeStart;
    } else {
      return null;
    }
  });

  BoutPhase? at(double time) {
    return items.maxByOrNull((a) {

      if(a.start <= time && (a.end ?? double.infinity) > time) {
        return a.realTimeStart;
      } else {
        return null;
      }
    });
  }
}