// ignore_for_file: file_names

import 'dart:convert';

import 'package:refflow/data/bout/Fencer.dart';
import 'package:refflow/util/enum.dart';

class BoutMetadata {


  String id = "";
  String name = "";
  String description = "";
  double started = 0;
  int lastUpdated = 0;
  BoutMetadataFencer left = BoutMetadataFencer();
  BoutMetadataFencer right = BoutMetadataFencer();

  BoutMetadata({
    this.id = "",
    this.name = "",
    this.description = "",
    this.started = 0,
    this.lastUpdated = 0,
    required this.left,
    required this.right,
  });
  BoutMetadata.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toString() as String;
    name = json['name']?.toString() as String;
    description = json['description']?.toString() as String;
    started = json['started']?.toDouble();
    lastUpdated = json['lastUpdated']?.toInt();
    left = (json['left'] != null) ? BoutMetadataFencer.fromJson(json['left']) : BoutMetadataFencer();
    right = (json['right'] != null) ? BoutMetadataFencer.fromJson(json['right']) : BoutMetadataFencer();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['started'] = started;
    data['lastUpdated'] = lastUpdated;
    data['left'] = left.toJson();
    data['right'] = right.toJson();
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }

  BoutMetadataFencer fencer(Fencer fencer) {
    switch(fencer) {
      case Fencer.left:
        return left;
      case Fencer.right:
        return right;
    }
  }
}
