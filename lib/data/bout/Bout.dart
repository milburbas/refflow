// ignore_for_file: file_names

import 'dart:convert';

import 'package:refflow/data/bout/Fencer.dart';
import 'package:refflow/util/doubles.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/lists.dart';

import 'Cards.dart';
import 'Format.dart';
import 'Metadata.dart';
import 'NonComs.dart';
import 'Phases.dart';
import 'Score.dart';


class Bout {


  BoutFormat format = BoutFormat();
  BoutScore score = BoutScore(touches: []);
  BoutPhases phases = BoutPhases(items: []);
  BoutCards cards = BoutCards(items: []);
  BoutNoncoms noncoms = BoutNoncoms(items: []);
  BoutMetadata metadata = BoutMetadata(left: BoutMetadataFencer(), right: BoutMetadataFencer());
  double time = 0;
  Fencer? priority;
  Fencer? winner;

  Bout({
    required this.format,
    required this.score,
    required this.phases,
    required this.cards,
    required this.noncoms,
    required this.metadata,
    this.time = 0,
    this.priority,
    this.winner,
  });
  Bout.fromJson(Map<String, dynamic> json) {
    format = (json['format'] != null) ? BoutFormat.fromJson(json['format']) : BoutFormat();
    score = (json['score'] != null) ? BoutScore.fromJson(json['score']) : BoutScore(touches: []);
    phases = (json['phases'] != null) ? BoutPhases.fromJson(json['phases']) : BoutPhases(items: []);
    cards = (json['cards'] != null) ? BoutCards.fromJson(json['cards']) : BoutCards(items: []);
    noncoms = (json['noncoms'] != null) ? BoutNoncoms.fromJson(json['noncoms']) : BoutNoncoms(items: []);
    metadata = (json['metadata'] != null) ? BoutMetadata.fromJson(json['metadata']) : BoutMetadata(left: BoutMetadataFencer(), right: BoutMetadataFencer());
    time = json['time']?.toDouble() ?? 0.0;
    priority = Fencer.values.firstWhereOrNull((element) => json['priority']?.toString().endsWith(element.name) ?? false);;
    winner = Fencer.values.firstWhereOrNull((element) => json['winner']?.toString().endsWith(element.name) ?? false);;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['format'] = format.toJson();
    data['score'] = score.toJson();
    data['phases'] = phases.toJson();
    data['cards'] = cards.toJson();
    data['noncoms'] = noncoms.toJson();
    data['metadata'] = metadata.toJson();
    data['time'] = time.roundTo(3);
    data['priority'] = priority.toString();
    data['winner'] = winner.toString();
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }

  BoutScoreTouch? get lastTouch => score.touches.maxByOrNull((item) => item.time);

  double get lastTouchTime => lastTouch != null ? lastTouch!.time : 0.0;

  int get touchGoal {
    if (format.freefencing) {
      return double.infinity.toInt();
    } else {
      return format.touchesToWin;
    }
  }
}


Bout get defaultBout => Bout(format: BoutFormat(),
  score: BoutScore(touches: []),
  phases: BoutPhases(items: [BoutPhase(realTimeStart: DateTime.now().millisecondsSinceEpoch, start: 0.0, type: PhaseType.period)]),
  cards: BoutCards(items: []),
  noncoms: BoutNoncoms(items: []),
  metadata: BoutMetadata(left: BoutMetadataFencer(), right: BoutMetadataFencer())
);