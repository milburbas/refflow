import 'package:flutter/cupertino.dart';
import 'package:refflow/util/enum.dart';

import '../../api.dart';
import '../../stopwatch_stream.dart';
import 'Bout.dart';
import 'Phases.dart';
import 'model.dart';

class BoutInfoModel extends ChangeNotifier {
  Bout? bout;
  API api = API();

  final timer = StopwatchStream();

  late TimeCN time = TimeCN(getBout: () => bout ?? defaultBout, timer: timer, updateProfiling: (_){} );

  DeltaType deltaType = DeltaType.touch;
  double get delta {
    switch (deltaType) {
      case DeltaType.touch:
        return time.touchDelta;
      case DeltaType.start:
        return time.startDelta;
    }
  }

  BoutInfoModel({String? id, this.bout}) {

    int _lastUpdate = DateTime.now().millisecondsSinceEpoch;

    if (bout?.metadata.id == null && id == null) {
    } else {
      String boutId = bout?.metadata.id ?? id!;
      api.get(boutId).then((result) {
        int now = DateTime.now().millisecondsSinceEpoch;
        bout = result ?? bout;
        notifyListeners();
        if (result != null) _lastUpdate = now;
      });
      api.watchBout(boutId).listen((event) {
        int now = DateTime.now().millisecondsSinceEpoch;
        bout = event;
        notifyListeners();
        _lastUpdate = now;
      });
    }


    void update() {
      if (bout?.metadata.id == null && id == null) {
        Future.delayed(const Duration(seconds: 1), update);
      } else {
        int now = DateTime.now().millisecondsSinceEpoch;
        String boutId = bout?.metadata.id ?? id!;
        if (now - _lastUpdate > 10000) {
          api.get(boutId).then((result) {
            int now = DateTime.now().millisecondsSinceEpoch;
            bout = result ?? bout;
            notifyListeners();
            if (result != null) _lastUpdate = now;
            Future.delayed(const Duration(seconds: 1), update);
          });
        } else {
          Future.delayed(const Duration(seconds: 1), update);
        }
      }
    }

    update();
  }

  BoutPhase get currentPhase {
    BoutPhase? result = bout?.phases.at(bout!.time);
    if (result == null) {
      return BoutPhase(realTimeStart: DateTime.now().millisecondsSinceEpoch, start: 0, type: PhaseType.period);
    } else {
      return result;
    }
  }
}