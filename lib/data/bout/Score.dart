// ignore_for_file: file_names

import 'dart:convert';
import 'dart:math';

import 'package:refflow/util/doubles.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/lists.dart';


class BoutScoreTouch {


  Fencer? scorer;
  double time = 0;
  int realtime = 0;

  BoutScoreTouch({
    required this.scorer,
    required this.time,
    required this.realtime,
  });
  BoutScoreTouch.fromJson(Map<String, dynamic> json) {
    scorer = Fencer.values.firstWhereOrNull((element) => json['scorer']?.toString().endsWith(element.name) ?? false);
    time = json['time']?.toDouble();
    realtime = json['realtime']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['scorer'] = scorer.toString();
    data['time'] = time.roundTo(3);
    data['realtime'] = realtime;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}

class BoutScore {

  List<BoutScoreTouch> _touches = List.empty();

  List<BoutScoreTouch> get touches {
    _touches.sort((a,b) => a.time.compareTo(b.time) );
    return _touches;
  }

  set touches(List<BoutScoreTouch> touches) {
    _touches = touches;
    _touches.sort((a,b) => a.time.compareTo(b.time) );
  }

  BoutScore({
    required List<BoutScoreTouch> touches,
  }) {
    _touches = touches;
  }
  BoutScore.fromJson(Map<String, dynamic> json) {
    if (json['touches'] != null) {
      final v = json['touches'];
      final arr0 = <BoutScoreTouch>[];
      v.forEach((v) {
        arr0.add(BoutScoreTouch.fromJson(v));
      });
      touches = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    final v = touches;
    final arr0 = [];
    for (var v in v) {
      arr0.add(v.toJson());
    }
    data['touches'] = arr0;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }

  int of(Fencer fencer) {
    int score = 0;
    for (var element in touches) {
        if(element.scorer == fencer || element.scorer == null) {
          score++;
        }
      }
    return score;
  }

  int get minScore => min(of(Fencer.left), of(Fencer.right));

  int get maxScore => max(of(Fencer.left), of(Fencer.right));

  Fencer? get trailing {
    int leftScore = of(Fencer.left);
    int rightScore = of(Fencer.right);
    if (leftScore > rightScore) {
      return Fencer.right;
    } else if (rightScore > leftScore) {
      return Fencer.left;
    } else {
      return null;
    }
  }

  Fencer? get leading {
    int leftScore = of(Fencer.left);
    int rightScore = of(Fencer.right);
    if (leftScore > rightScore) {
      return Fencer.left;
    } else if (rightScore > leftScore) {
      return Fencer.right;
    } else {
      return null;
    }
  }
}
