// ignore_for_file: file_names

import 'dart:convert';

import 'package:refflow/util/doubles.dart';
import 'package:refflow/util/lists.dart';

import '../../util/enum.dart';

class BoutNoncoms {

  List<BoutNoncom> items = List.empty();

  BoutNoncoms({
    required this.items,
  });
  BoutNoncoms.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      final v = json['items'];
      final arr0 = <BoutNoncom>[];
      v.forEach((v) {
        arr0.add(BoutNoncom.fromJson(v));
      });
      items = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    final v = items;
    final arr0 = [];
    for (var v in v) {
      arr0.add(v.toJson());
    }
    data['items'] = arr0;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}

class BoutNoncom {

  Fencer? scorer;
  double time = 0;
  int realtime = 0;

  BoutNoncom({
    required this.scorer,
    required this.time,
    required this.realtime,
  });
  BoutNoncom.fromJson(Map<String, dynamic> json) {
    scorer = Fencer.values.firstWhereOrNull((element) => json['scorer']?.toString().endsWith(element.name) ?? false);
    time = json['time']?.toDouble();
    realtime = json['realtime']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['scorer'] = scorer.toString();
    data['time'] = time.roundTo(3);
    data['realtime'] = realtime;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}