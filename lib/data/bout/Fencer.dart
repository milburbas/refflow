// ignore_for_file: file_names

import 'dart:convert';

class BoutMetadataFencer {
  String? id;
  String? name;

  BoutMetadataFencer({
    this.id,
    this.name,
  });
  BoutMetadataFencer.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toString();
    name = json['name']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}