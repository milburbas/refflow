// ignore_for_file: file_names

import 'dart:convert';

import 'package:refflow/util/doubles.dart';
import 'package:refflow/util/lists.dart';

import '../../util/enum.dart';

class BoutCard {

  Fencer? scorer;
  CardType? type;
  bool P = false;
  double time = 0;
  int realtime = 0;

  BoutCard({
    required this.scorer,
    required this.type,
    required this.P,
    required this.time,
    required this.realtime,
  });
  BoutCard.fromJson(Map<String, dynamic> json) {
    scorer = Fencer.values.firstWhereOrNull((element) => json['scorer']?.toString().endsWith(element.name) ?? false);
    type = CardType.values.firstWhere((element) => json['type']?.toString().endsWith(element.name) ?? false, orElse: () => CardType.yellow);
    P = json['P'];
    time = json['time']?.toDouble();
    realtime = json['realtime']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['scorer'] = scorer.toString();
    data['type'] = type.toString();
    data['P'] = P;
    data['time'] = time.roundTo(3);
    data['realtime'] = realtime;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}

class BoutCards {

  List<BoutCard> items = List.empty();

  BoutCards({
    required this.items,
  });
  BoutCards.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      final v = json['items'];
      final arr0 = <BoutCard>[];
      v.forEach((v) {
        arr0.add(BoutCard.fromJson(v));
      });
      items = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    final v = items;
    final arr0 = [];
    for (var v in v) {
      arr0.add(v.toJson());
    }
    data['items'] = arr0;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}