
import 'dart:convert';
// import 'dart:html';
import 'dart:math';
import 'dart:developer' as dev;

import 'package:flutter/cupertino.dart';
import 'package:refflow/data/bout/Bout.dart';
import 'package:refflow/data/bout/Cards.dart';
import 'package:refflow/data/bout/Format.dart';
import 'package:refflow/data/bout/NonComs.dart';
import 'package:refflow/data/bout/Phases.dart';
import 'package:refflow/data/bout/Score.dart';
import 'package:refflow/stopwatch_stream.dart';
import 'package:refflow/util/doubles.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/lists.dart';
import 'package:vibration/vibration.dart';

import '../../api.dart';


class BoutModel extends ChangeNotifier {
  Bout bout;
  API api = API();
  final timer = StopwatchStream();

  int timerChecks = 0;
  int phaseChecks = 0;
  int scoreChecks = 0;

  double timerCheckTime = 0.0;
  double phaseCheckTime = 0.0;
  double scoreCheckTime = 0.0;

  BoutModel({
        required this.bout
      }) {
    timer.stream.listen((event) {
      checkTimer();
      checkScore(false);
    });
    int lastPut = DateTime.now().millisecondsSinceEpoch;
    addListener(() {
      bout.metadata.lastUpdated = DateTime.now().millisecondsSinceEpoch;
      if (DateTime.now().millisecondsSinceEpoch - lastPut > 200) {
        lastPut = DateTime.now().millisecondsSinceEpoch;
        Future.delayed(const Duration(milliseconds: 200), () {
          api.upload(bout).then((id) => bout.metadata.id = id ?? bout.metadata.id);
        });
      }
      // window.localStorage['bout'] = json.encode(bout.toJson());
      time.notifyListeners();
    });
  }

  late TimeCN time = TimeCN(getBout: () => bout, timer: timer, updateProfiling: (elapsed) {timerCheckTime+=elapsed;timerChecks++;});
  
  BoutPhase get currentPhase {
    BoutPhase? result = bout.phases.at(bout.time);
    if (result == null) {
      bout.phases.items.add(BoutPhase(realTimeStart: DateTime.now().millisecondsSinceEpoch, start: bout.time, type: PhaseType.period));
      return bout.phases.at(bout.time)!;
    } else {
      return result;
    }
  }

  bool _showHelp = true;

  bool get showHelp => _showHelp;

  set showHelp(value) {
    _showHelp = value;
    notifyListeners();
  }

  DeltaType deltaType = DeltaType.touch;
  double get delta {
    switch (deltaType) {
      case DeltaType.touch:
        return time.touchDelta;
      case DeltaType.start:
        return time.startDelta;
    }
  }

  bool isReversed = false;

  void reverse() {
    isReversed = !isReversed;
    notifyListeners();
  }

  void reset({BoutFormat? newFormat}) {
    stop();
    Bout newBout = defaultBout;
    if (newFormat != null) {
      newBout.format = newFormat;
    } else {
      newBout.format = bout.format;
    }
    bout = newBout;
    showHelp = false;
    notifyListeners();
  }

  void assignPriority() {
    int priorityId = Random().nextInt(2);
    Fencer recipient = Fencer.values[priorityId];
    bout.priority = recipient;
    notifyListeners();
  }
  
  void clearPriority() {
    bout.priority = null;
    notifyListeners();
  }
  
  void toFreefencing() {
    bout.format.freefencing = true;
    notifyListeners();
  }
  
  void startIntermission(double length) {
    stop();
    bout.phases.items.add(BoutPhase(realTimeStart: DateTime.now().millisecondsSinceEpoch, start: bout.time, type: PhaseType.intermission, length: length));
  }

  void endPhase() {
    stop();
    BoutPhase localCurrentPhase = currentPhase;
    localCurrentPhase.end = bout.time;
    bool isLastPeriod = bout.phases.period >= bout.format.periods && !bout.format.freefencing;
    if (localCurrentPhase.type == PhaseType.period && isLastPeriod) {
      checkScore(true);
    }
    if (localCurrentPhase.type == PhaseType.overtime) {
      touch(bout.priority);
    }
    PhaseType? nextPhaseType;
    switch (localCurrentPhase.type) {
      case PhaseType.period:
        if (bout.format.freefencing) {
          nextPhaseType = PhaseType.period;
        } else if (!isLastPeriod) {
          nextPhaseType = PhaseType.break_;
        } else if (bout.winner == null) {
          nextPhaseType = PhaseType.overtime;
        } else {
          nextPhaseType = null;
        }
        break;
      case PhaseType.break_:
        nextPhaseType = PhaseType.period;
        break;
      case PhaseType.overtime:
        nextPhaseType = null;
        break;
      case PhaseType.intermission:
        nextPhaseType = null;
        break;
    }
    if (nextPhaseType != null) {
      if (nextPhaseType == PhaseType.overtime) {
        assignPriority();
      }
      bout.phases.items.add(BoutPhase(realTimeStart: DateTime.now().millisecondsSinceEpoch, start: bout.time, type: nextPhaseType));
    }
    notifyListeners();
  }

  double? get desiredLength {
    switch (currentPhase.type) {
      case PhaseType.period:
        if (!bout.format.freefencing) {
          return bout.format.timePerPeriod;
        } else {
          return null;
        }
      case PhaseType.break_:
        return bout.format.timePerBreak;
      case PhaseType.overtime:
        return bout.format.overtimeLength;
      case PhaseType.intermission:
        return currentPhase.length;
    }
  }

  void checkScore(bool forceOvertime) {
    int startTime = DateTime.now().millisecondsSinceEpoch;
    bool scoreIsEqual = bout.score.leading == null;
    int touchGoal;
    if ((currentPhase.type == PhaseType.overtime || forceOvertime) && !scoreIsEqual) {
      touchGoal = bout.score.of(bout.score.leading!);
    } else {
      touchGoal = bout.touchGoal;
    }
    if (bout.score.of(Fencer.left) >= touchGoal) {
      bout.winner = Fencer.left;
    } else if (bout.score.of(Fencer.right) >= touchGoal) {
      bout.winner = Fencer.right;
    } else {
      bout.winner = null;
    }
    int endTime = DateTime.now().millisecondsSinceEpoch;
    int elapsed = endTime - startTime;
    scoreCheckTime += elapsed;
    scoreChecks++;
  }

  double lastContinuousUpdate = 0.0;
  void checkTimer() {

    int startTime = DateTime.now().millisecondsSinceEpoch;

    if (bout.time - lastContinuousUpdate > 5) {
      api.upload(bout).then((id) => bout.metadata.id = id ?? bout.metadata.id);
      lastContinuousUpdate = bout.time;
    }

    BoutPhase localCurrentPhase = currentPhase;

    if (localCurrentPhase.type == PhaseType.period) {
      double noncomDelta =  bout.time - (
          bout.noncoms.items.where( (a) =>
            a.time <= bout.time
          ).toList().maxByOrNull( (a) =>
            a.time
          )?.time ?? double.negativeInfinity
      );
      if (min(time.touchDelta, noncomDelta) > (bout.format.nonCom?.toDouble() ?? double.infinity)) {
        nonCom(bout.score.trailing);
      }
    }

    switch(localCurrentPhase.type) {
      case PhaseType.period:
        if (!bout.format.freefencing) {
          double localDesiredLength = desiredLength!;
          double actualLength = bout.phases.escapedLength(localCurrentPhase.start, bout.time, localCurrentPhase.realTimeStart.toDouble());
          double diff = actualLength - localDesiredLength;
          if (diff > 0) {
            time.setTime(-diff, force: true);
            endPhase();
          }
        }
        break;
      case PhaseType.break_:
        double localDesiredLength = desiredLength!;
        double actualLength = bout.phases.escapedLength(localCurrentPhase.start, bout.time, localCurrentPhase.realTimeStart.toDouble());
        double diff = actualLength - localDesiredLength;
        if (diff > 0) {
          time.setTime(-diff, force: true);
          endPhase();
        }
        break;
      case PhaseType.overtime:
        double localDesiredLength = desiredLength!;
        double actualLength = bout.phases.escapedLength(localCurrentPhase.start, bout.time, localCurrentPhase.realTimeStart.toDouble());
        double diff = actualLength - localDesiredLength;
        if (diff > 0) {
          time.setTime(-diff, force: true);
          endPhase();
        }
        break;
      case PhaseType.intermission:
        double localDesiredLength = desiredLength!;
        double actualLength = bout.phases.escapedLength(localCurrentPhase.start, bout.time, localCurrentPhase.realTimeStart.toDouble());
        double diff = actualLength - localDesiredLength;
        if (diff > 0) {
          time.setTime(-diff, force: true);
          endPhase();
        }
        break;
    }
    int endTime = DateTime.now().millisecondsSinceEpoch;
    int elapsed = endTime - startTime;
    phaseCheckTime += elapsed;
    phaseChecks++;
  }

  void start() {
    timer.startTimer();
    notifyListeners();
  }

  void stop() {
    timer.stopTimer();
    notifyListeners();
  }

  void toggle({ bool silent = true }) {
    timer.toggleTimer();
    notifyListeners();
    if (!silent) {
      Vibration.vibrate(
          duration: 200
      );
    }
  }
  
  void touch(Fencer? fencer, { bool silent = true }) {
    stop();
    bout.score.touches.add(BoutScoreTouch(scorer: fencer, time: bout.time, realtime: DateTime.now().millisecondsSinceEpoch));
    checkScore(false);
    notifyListeners();
    if (!silent) {
      switch(fencer) {
        case Fencer.left:
          Vibration.vibrate(
            pattern: [50,25,50,25,50]
          );
          break;
        case Fencer.right:
          Vibration.vibrate(
              pattern: [100,10,100,10,100]
          );
          break;
        case null:
          Vibration.vibrate(
              pattern: [50, 25, 50, 10, 100, 10, 100]
          );
          break;
      }
    }
  }

  CardType card(Fencer? fencer, CardType type, { bool silent = true, bool P = false }) {
    stop();
    List<BoutCard> currentCards = bout.cards.items.where((card) => (card.scorer == fencer || card.scorer == null) && card.time <= bout.time).toList();
    int yellowCards = currentCards.count((card) => card.type == CardType.yellow && card.P == P);
    if (yellowCards >= 1) {
      type = CardType.red;
    }
    bout.cards.items.add(BoutCard(scorer: fencer, type: type, P: P, time: bout.time, realtime: DateTime.now().millisecondsSinceEpoch));
    switch(type) {
      case CardType.yellow:
        Vibration.vibrate(
            pattern: [50,25,50,25,50]
        );
        break;
      case CardType.red:
        Fencer? otherFencer;
        switch (fencer) {
          case Fencer.left:
            otherFencer = Fencer.right;
            break;
          case Fencer.right:
            otherFencer = Fencer.left;
            break;
          case null:
            otherFencer = null;
            break;
        }
        touch(
          otherFencer,
          silent: true
        );
        break;
      case CardType.black:
        break;
    }
    notifyListeners();
    if (!silent) {
      Vibration.vibrate(
          duration: 300
      );
    }
    notifyListeners();
    return type;
  }

  void nonCom(Fencer? fencer) {
    stop();
    bout.noncoms.items.add(BoutNoncom(scorer: fencer, time: bout.time, realtime: DateTime.now().millisecondsSinceEpoch));
    card(fencer, CardType.yellow, P: true);
    notifyListeners();
  }


  void undoTouch(Fencer? fencer, {bool force = false}) {
    stop();
    List<BoutScoreTouch> touches = bout.score.touches.toList();
    touches.sort((a,b) => a.realtime.compareTo(b.realtime));
    try {
      BoutScoreTouch target = touches.lastWhere((element) =>
        force || (element.scorer != null ? element.scorer == fencer : true)
      );
      if (target.scorer == null && fencer != null && !force) {
        int targetIndex = bout.score.touches.indexOf(target);
        bout.score.touches.elementAt(targetIndex).scorer = fencer == Fencer.left ? Fencer.right : Fencer.left;
      } else {
        bout.score.touches.remove(target);
      }
      checkScore(false);
      notifyListeners();
    } catch (err) {}
  }
}

class TimeCN extends ChangeNotifier {
  Bout Function() getBout;
  StopwatchStream timer;
  void Function(double) updateProfiling;

  TimeCN({
    required this.getBout,
    required this.timer,
    required this.updateProfiling
  }) {
    timer.stream.listen((event) {setTime(event);});
    addListener(() {
      int startTime = DateTime.now().millisecondsSinceEpoch;
      minutes.check();
      seconds.check();
      centiseconds.check();
      int endTime = DateTime.now().millisecondsSinceEpoch;
      int elapsed = endTime - startTime;
      updateProfiling(elapsed.toDouble());
    });
  }

  late MinutesCN minutes = MinutesCN(
      timer: timer,
      getDisplayTime: () => displayTime,
    getStartDelta: () => startDelta,
    getTouchDelta: () => touchDelta,
  );
  late SecondsCN seconds = SecondsCN(
      timer: timer,
      getDisplayTime: () => displayTime,
    getStartDelta: () => startDelta,
    getTouchDelta: () => touchDelta,
  );
  late CentisecondsCN centiseconds = CentisecondsCN(
      timer: timer,
      getDisplayTime: () => displayTime,
      getStartDelta: () => startDelta,
      getTouchDelta: () => touchDelta,
  );

  BoutPhase get currentPhase {
    BoutPhase? result = getBout().phases.at(getBout().time);
    if (result == null) {
      getBout().phases.items.add(BoutPhase(realTimeStart: DateTime.now().millisecondsSinceEpoch, start: getBout().time, type: PhaseType.period));
      return getBout().phases.at(getBout().time)!;
    } else {
      return result;
    }
  }

  double? get desiredLength {
    switch (currentPhase.type) {
      case PhaseType.period:
        if (!getBout().format.freefencing) {
          return getBout().format.timePerPeriod;
        } else {
          return null;
        }
      case PhaseType.break_:
        return getBout().format.timePerBreak;
      case PhaseType.overtime:
        return getBout().format.overtimeLength;
      case PhaseType.intermission:
        return currentPhase.length;
    }
  }

  double get time => getBout().phases.escapedLength(currentPhase.start, getBout().time, currentPhase.realTimeStart.toDouble());
  double get touchDelta => getBout().phases.escapedLength(
      [
        currentPhase.start,
        // getBout().noncoms.items.maxByOrNull((a) => a.time)?.time ?? double.negativeInfinity,
        getBout().lastTouchTime
      ].maxByOrNull((a) => a)!,
      getBout().time,
      [
        currentPhase.realTimeStart.toDouble(),
        // getBout().noncoms.items.maxByOrNull((a) => a.realtime)?.realtime.toDouble() ?? double.negativeInfinity,
        getBout().lastTouch?.realtime.toDouble() ?? double.negativeInfinity
      ].maxByOrNull((a) => a)!
  );
  double get startDelta => getBout().time - timer.lastStartTime;
  double get displayTime {
    double? localDesiredLength = desiredLength;
    double output;
    if (localDesiredLength != null) {
      output = localDesiredLength - time;
    } else {
      output = time;
    }
    return output.roundTo(2, ceil: true);
  }

  void setTime(double delta, {bool force = false}) {
    bool valid = timer.isActive();
    if (force) valid = true;
    if (valid) {
      Bout bout = getBout();
      bout.time += delta;
      notifyListeners();
    }
  }

}

class MinutesCN extends ChangeNotifier {
  StopwatchStream timer;
  Function getDisplayTime;
  Function getStartDelta;
  Function getTouchDelta;

  int get value => (getDisplayTime() / 60).floor();

  String get string {
    return value.toString();
  }

  int get startDeltaValue => (getStartDelta() / 60).floor();

  String get startDeltaString {
    return startDeltaValue.toString();
  }

  int get touchDeltaValue => (getTouchDelta() / 60).floor();

  String get touchDeltaString {
    return touchDeltaValue.toString();
  }


  MinutesCN({
    required this.timer,
    required this.getDisplayTime,
    required this.getStartDelta,
    required this.getTouchDelta,
  }) {
    timer.stream.listen((_) {check();});
  }


  int lastValue = 0;
  void check() {
    if (value != lastValue) {
      lastValue = value;
      notifyListeners();
    }
  }

}

class SecondsCN extends ChangeNotifier {
  StopwatchStream timer;
  Function getDisplayTime;
  Function getStartDelta;
  Function getTouchDelta;

  int get value => (getDisplayTime() % 60).floor();

  String get string {
    String noPad = value.toString();
    if (noPad.length < 2) {
      return "0" + noPad;
    } else {
      return noPad;
    }
  }

  int get startDeltaValue => (getStartDelta() % 60).floor();

  String get startDeltaString {
    String noPad = startDeltaValue.toString();
    if (noPad.length < 2) {
      return "0" + noPad;
    } else {
      return noPad;
    }
  }

  int get touchDeltaValue => (getTouchDelta() % 60).floor();

  String get touchDeltaString {
    String noPad = touchDeltaValue.toString();
    if (noPad.length < 2) {
      return "0" + noPad;
    } else {
      return noPad;
    }
  }

  SecondsCN({
    required this.timer,
    required this.getDisplayTime,
    required this.getStartDelta,
    required this.getTouchDelta,
  }) {
    timer.stream.listen((_) {check();});
  }


  int lastValue = 0;
  void check() {
    if (value != lastValue) {
      if (value % 5 == 0) {
        Vibration.vibrate(duration: 200);
      } else {
        Vibration.vibrate(duration: 25);
      }
      lastValue = value;
      notifyListeners();
    }
  }

}

class CentisecondsCN extends ChangeNotifier {
  StopwatchStream timer;
  Function getDisplayTime;
  Function getStartDelta;
  Function getTouchDelta;

  int get value {
    double displayTime = getDisplayTime();
    return ((displayTime - displayTime.floor()) * 100).floor();
  }

  String get string {
    String noPad = value.toString();
    if (noPad.length < 2) {
      return "0" + noPad;
    } else {
      return noPad;
    }
  }

  int get startDeltaValue {
    double startDelta = getStartDelta();
    return ((startDelta - startDelta.floor()) * 100).floor();
  }

  String get startDeltaString {
    String noPad = startDeltaValue.toString();
    if (noPad.length < 2) {
      return "0" + noPad;
    } else {
      return noPad;
    }
  }

  int get touchDeltaValue {
    double touchDelta = getTouchDelta();
    return ((touchDelta - touchDelta.floor()) * 100).floor();
  }

  String get touchDeltaString {
    String noPad = touchDeltaValue.toString();
    if (noPad.length < 2) {
      return "0" + noPad;
    } else {
      return noPad;
    }
  }

  CentisecondsCN({
    required this.timer,
    required this.getDisplayTime,
    required this.getStartDelta,
    required this.getTouchDelta,
  }) {
    timer.stream.listen((_) {check();});
  }


  int lastValue = 0;
  void check() {
    if ((value - lastValue).abs() > 3) {
      lastValue = value;
      notifyListeners();
    }
  }

}
