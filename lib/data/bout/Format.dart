// ignore_for_file: file_names

import 'dart:convert';

import 'package:refflow/util/doubles.dart';

class BoutFormat {

  bool freefencing = false;
  double timePerPeriod = 180;
  double overtimeLength = 60;
  double timePerBreak = 60;
  int touchesToWin = 15;
  int periods = 3;
  int? nonCom;

  BoutFormat({
    this.freefencing = false,
    this.timePerPeriod = 180,
    this.overtimeLength = 60,
    this.timePerBreak = 60,
    this.touchesToWin = 15,
    this.periods = 3,
    this.nonCom
  });
  BoutFormat.fromJson(Map<String, dynamic> json) {
    freefencing = json['freefencing'];
    timePerPeriod = json['timePerPeriod'].toDouble();
    overtimeLength = json['overtimeLength'].toDouble();
    timePerBreak = json['timePerBreak'].toDouble();
    touchesToWin = json['touchesToWin'].toInt();
    periods = json['periods'].toInt();
    nonCom = json['nonCom']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['freefencing'] = freefencing;
    data['timePerPeriod'] = timePerPeriod.roundTo(3);
    data['overtimeLength'] = overtimeLength.roundTo(3);
    data['timePerBreak'] = timePerBreak.roundTo(3);
    data['touchesToWin'] = touchesToWin;
    data['periods'] = periods;
    data['nonCom'] = nonCom;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}