import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:refflow/bout_info/time_score.dart';
import 'package:refflow/bout_info/touch_list.dart';
import 'package:refflow/data/bout/info_model.dart';
import 'package:refflow/util/enum.dart';
import 'package:refflow/util/lists.dart';

import 'bout/score_display.dart';
import 'bout_screen.dart';
import 'data/bout/Bout.dart';
import 'data/bout/NonComs.dart';
import 'generated/l10n.dart';
import 'new_bout_screen.dart';

class BoutInfoScreenArguments {
  final String? boutString;
  final String? id;

  BoutInfoScreenArguments({this.boutString, this.id});

}

class BoutInfoScreen extends StatelessWidget {

  final Bout? bout;
  final String? id;

  const BoutInfoScreen({
          Key? key,
          this.bout,
          this.id
        }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final BoutInfoModel boutInfoModel = BoutInfoModel(id: id, bout: bout);
    Widget child = Scaffold(
        backgroundColor: Colors.black,
        body: Consumer<BoutInfoModel> (
          builder: (context, boutInfoModel, child) {
            Bout bout = boutInfoModel.bout ?? defaultBout;
            return Column(
              children: [
                timeAndScore(
                    () {
                      Future.microtask(() => Navigator.pushNamed(context, '/new', arguments: NewBoutScreenArguments(formatString: boutInfoModel.bout?.format.toString())));
                    },
                    bout.metadata.id,
                    bout.format.touchesToWin,
                    bout.phases.period,
                    bout.format.periods,
                    boutInfoModel.time.time,
                    boutInfoModel.delta,
                    boutInfoModel.deltaType,
                    boutInfoModel.timer.isActive(),
                    bout.score.of(Fencer.left),
                    bout.score.of(Fencer.right),
                    bout.metadata.fencer(Fencer.left).name,
                    bout.metadata.fencer(Fencer.right).name,
                    bout.priority,
                    bout.winner,
                    false,
                    []
                ),
                touchList(bout.score.touches, bout.time)
              ],
            );
          },
        )
    );
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => boutInfoModel,
        ),
        ChangeNotifierProvider(
          create: (context) => boutInfoModel.time,
        ),
        ChangeNotifierProvider(
          create: (context) => boutInfoModel.time.minutes,
        ),
        ChangeNotifierProvider(
          create: (context) => boutInfoModel.time.seconds,
        ),
        ChangeNotifierProvider(
          create: (context) => boutInfoModel.time.centiseconds,
        )
      ],
      child: child,
    );
  }

}

List<String> assembleMessages(BoutInfoModel boutInfoModel) {
  List<String> messages = [];

  if (boutInfoModel.bout != null) {
    switch (boutInfoModel.bout!.winner) {
      case Fencer.left:
        messages.add(S.current.winMessage("left"));
        break;
      case Fencer.right:
        messages.add(S.current.winMessage("right"));
        break;
      default:
        break;
    }
    BoutNoncom? lastNonCom =
        boutInfoModel.bout!.noncoms.items.maxByOrNull((a) => a.time);

    if (lastNonCom != null) {
      if (boutInfoModel.bout!.time - lastNonCom.time < 10) {
        messages.add(S.current.noncombativity);
      }
    }

    switch (boutInfoModel.currentPhase.type) {
      case PhaseType.period:
        break;
      case PhaseType.break_:
        messages.add(S.current.breakMessage);
        break;
      case PhaseType.overtime:
        messages.add(S.current.overtime);
        break;
      case PhaseType.intermission:
        messages.add(S.current.intermission);
        break;
    }
    switch (boutInfoModel.bout!.priority) {
      case Fencer.left:
        messages.add(S.current.priorityLeft);
        break;
      case Fencer.right:
        messages.add(S.current.priorityRight);
        break;
      default:
        break;
    }
  }
  return messages;
}