import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:refflow/refflow_icons.dart';

Widget noticeBar(
    List<String> messages
    ) {
  List<Widget> children = [];
  int idx = -1;
  for (String message in messages) {
    idx++;
    if (idx > 0) {
      children.add(
        const Icon(
          RefflowIcons.circle,
          size: 10,
        )
      );
    }
    children.add(
        Text(
            message,
          textScaleFactor: 1.2,
        )
    );
  }
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: SizedBox(
      height: 20,
      child: Row(
        children: children,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
    ),
  );
}