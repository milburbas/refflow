import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:refflow/bout/notice_bar.dart';
import 'package:refflow/bout/quick_action_bar.dart';
import 'package:refflow/bout/score_display.dart';
import 'package:refflow/bout/time_row.dart';
import 'package:refflow/refflow_icons.dart';
import 'package:refflow/util/enum.dart';

import '../data/bout/Cards.dart';
import '../generated/l10n.dart';
import 'card_display.dart';



Widget infoPane (
    void Function() onResetPressed,
    void Function() onSwapPressed,
    void Function() onIntermissionPressed,
    void Function() onPriorityPressed,
    void Function() onNextPeriodPressed,
    void Function() onUndoPressed,
    void Function() onClearPriorityPressed,
    void Function() onNewBoutPressed,
    void Function() onFreefencingPressed,
    void Function() logProfilingPressed,
    void Function(Fencer, CardType) onCardPressed,
    String id,
    int touchGoal,
    int period,
    int? periods,
    double displayTime,
    double displayDelta,
    DeltaType deltaType,
    bool dropCentiseconds,
    int scoreLeft,
    int scoreRight,
    String? nameLeft,
    String? nameRight,
    Fencer? priority,
    Fencer? winner,
    List<BoutCard> cards,
    bool isReversed,
    List<String> messages
    ) {
  List<Widget> centralChildren = [
    GestureDetector(
        onTap: () => onCardPressed(Fencer.left, CardType.yellow),
        child: Container(
          decoration: const BoxDecoration(color: Color.fromARGB(255, 255, 255, 0), borderRadius: BorderRadius.all(Radius.circular(8))),
          height: 32,
          width: 32,
          child: Center(
              child: Text(
                S.current.yellowCardShort,
                style: const TextStyle(color: Colors.black, fontSize: 28),
              )
          ),
        )
    ),
    const Padding(padding: EdgeInsets.all(2)),
    GestureDetector(
        onTap: () => onCardPressed(Fencer.left, CardType.red),
        child: Container(
          decoration: const BoxDecoration(color: Color.fromARGB(255, 255, 0, 0), borderRadius: BorderRadius.all(Radius.circular(8))),
          height: 32,
          width: 32,
          child: Center(
              child: Text(
                S.current.redCardShort,
                style: const TextStyle(color: Colors.black, fontSize: 28),
              )
          ),
        )
    ),
    const Padding(padding: EdgeInsets.all(2)),
    touchGoalDisplay(touchGoal),
    const Padding(padding: EdgeInsets.all(2)),
    GestureDetector(
        onTap: () => onCardPressed(Fencer.right, CardType.red),
        child: Container(
          decoration: const BoxDecoration(color: Color.fromARGB(255, 255, 0, 0), borderRadius: BorderRadius.all(Radius.circular(8))),
          height: 32,
          width: 32,
          child: Center(
              child: Text(
                S.current.redCardShort,
                style: const TextStyle(color: Colors.black, fontSize: 28),
              )
          ),
        )
    ),
    const Padding(padding: EdgeInsets.all(2)),
    GestureDetector(
        onTap: () => onCardPressed(Fencer.right, CardType.yellow),
        child: Container(
            decoration: const BoxDecoration(color: Color.fromARGB(255, 255, 255, 0), borderRadius: BorderRadius.all(Radius.circular(8))),
            height: 32,
            width: 32,
          child: Center(
            child: Text(
              S.current.yellowCardShort,
              style: const TextStyle(color: Colors.black, fontSize: 28),
            )
          ),
        )
    ),
  ];
  if (isReversed) centralChildren = centralChildren.reversed.toList();
  List<Widget> children = [
    Column(
      children: [
        scoreDisplay(scoreLeft, nameLeft ?? S.current.leftNameDefault, priority == Fencer.left),
        cardDisplay(cards.where((element) => (element.scorer ?? Fencer.left) == Fencer.left).toList()),
      ],
    ),
    Column (
      children: <Widget>[
        quickActionBar(onResetPressed, onSwapPressed, onIntermissionPressed, onPriorityPressed),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: centralChildren,
        )
      ],
    ),
    Column(
      children: [
        scoreDisplay(scoreRight, nameRight ?? S.current.rightNameDefault, priority == Fencer.right),
        cardDisplay(cards.where((element) => (element.scorer ?? Fencer.right) == Fencer.right).toList()),
      ],
    ),
  ];
  if (isReversed) {
    children = children.reversed.toList();
  }

  return Column (
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Row(
        children: [
          Expanded(child: noticeBar(messages)),
          Builder(
            builder: (context) {
              List<PopupMenuEntry> items = [
                PopupMenuItem(
                  value: 1,
                  child: Text(S.of(context).undoTouch),
                  onTap: onUndoPressed,
                ),
                PopupMenuItem(
                  value: 2,
                  child: Text(S.of(context).resetBout),
                  onTap: onResetPressed,
                ),
                PopupMenuItem(
                  value: 3,
                  child: Text(S.of(context).nextPeriod),
                  onTap: onNextPeriodPressed,
                ),
                PopupMenuItem(
                  value: 4,
                  child: Text(S.of(context).priority),
                  onTap: onPriorityPressed,
                ),
                PopupMenuItem(
                  value: 5,
                  child: Text(S.of(context).clearPriority),
                  onTap: onClearPriorityPressed,
                ),
                PopupMenuItem(
                  value: 6,
                  child: Text(S.of(context).newBout),
                  onTap: onNewBoutPressed,
                ),
                PopupMenuItem(
                  value: 7,
                  child: Text(S.of(context).freeFencing),
                  onTap: onFreefencingPressed,
                ),
                PopupMenuItem(
                  value: 8,
                  child: Text("Log profiling data"),
                  onTap: logProfilingPressed,
                ),
              ];
              if (id != '') {
                items.add(
                    PopupMenuItem(
                      value: 8,
                      child: Text(id, style: const TextStyle(color: Colors.grey)),
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: id));
                      },
                    )
                );
              }
              return SizedBox(
                height: 36,
                child: PopupMenuButton(
                  itemBuilder: (context) => items,
                  icon: const Icon(RefflowIcons.menu),
                ),
              );
            }
          )
        ],
      ),
      timeRow(period = period, periods = periods, displayTime = displayTime, displayDelta = displayDelta, deltaType = deltaType, dropCentiseconds = dropCentiseconds),
      Row (
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: children
      ),
    ],
  );
}

Widget touchGoalDisplay (int? touchGoal) {
  String text = touchGoal != null ? S.current.touchGoal(touchGoal) : S.current.freefencing;
  return Text(text, textAlign: TextAlign.center,);
}