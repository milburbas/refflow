import 'package:flutter/material.dart';
import 'package:refflow/util/enum.dart';

import '../generated/l10n.dart';

Widget scorePane (
    void Function(Fencer?) touch,
    void Function(Fencer?) undo,
    void Function() timer,
    bool scoreEnabled,
    bool enabled,
    bool isReversed,
    bool showHelp,
    ) {
  Widget? leftHelp;
  Widget? timerHelp;
  Widget? rightHelp;
  if (showHelp) {
    leftHelp = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(S.current.pointLeft, textScaleFactor: 1.3, textAlign: TextAlign.center),
        const Text("\n", textScaleFactor: 1.3, textAlign: TextAlign.center),
        Text(S.current.holdToUndo, textScaleFactor: 1.3, textAlign: TextAlign.center),
      ],
    );
    timerHelp = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(S.current.startTimer, textScaleFactor: 1.3, textAlign: TextAlign.center),
      ],
    );
    rightHelp = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(S.current.pointRight, textScaleFactor: 1.3, textAlign: TextAlign.center),
        const Text("\n", textScaleFactor: 1.3, textAlign: TextAlign.center),
        Text(S.current.holdToUndo, textScaleFactor: 1.3, textAlign: TextAlign.center),
      ],
    );
  }
  List<Widget> children = [
    Expanded(
      child: InkWell(
        onTap: () { if (scoreEnabled) touch(Fencer.left); },
        onLongPress: () { undo(Fencer.left); },
        child: Center(
          child: leftHelp
        ),
      ),
    ),
    Expanded(
      flex: 2,
      child: InkWell(
        onTap: timer,
          child: Column(
            children: <Widget>[
              Expanded(
                  child: Center(
                    child: timerHelp
                  )
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: ElevatedButton(
                    onPressed: () { if (scoreEnabled) touch(null); },
                    onLongPress: () { undo(null); },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(S.current.doubleTouch, textScaleFactor: 1.6),
                    )
                ),
              )
            ],
          )
      ),
    ),
    Expanded(
      child: InkWell(
        onTap: () { if (scoreEnabled) touch(Fencer.right); },
        onLongPress: () { undo(Fencer.right); },
        child: Center(
            child: rightHelp
        ),
      ),
    ),
  ];
  if (isReversed) {
    children = children.reversed.toList();
  }
  if (!enabled) {
    children = [];
  }
  return Expanded(
    child: Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children
    ),
  );
}