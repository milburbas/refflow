import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:refflow/data/bout/model.dart';

import '../generated/l10n.dart';
import '../util/enum.dart';

Widget timeRow (
    int period,
    int? periods,
    double displayTime,
    double displayDelta,
    DeltaType deltaType,
    bool dropCentiseconds
    ) {

  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      periodDisplay(period, periods),
      timeDisplay(0, dropCentiseconds, 5),
      deltaDisplay(0, dropCentiseconds, 1, deltaType),
    ],
  );

}

Widget periodDisplay (int period, int? periods) {
  String text = periods != null ?
  S.current.periodOfPeriods(period, periods)
      :
  S.current.periodPeriod(period);
  return Text(
    text,
    textAlign: TextAlign.center,
    textScaleFactor: 1,
  );
}

Widget timeDisplay (double displayTime, bool dropCentiseconds, double textScale) {
  List<Widget> widgets = [
    Consumer<MinutesCN>(
      builder: (context, minutes, child) {
        return Text(
          minutes.string,
          textScaleFactor: textScale,
        );
      },
    ),
    Text(
      ":",
      textScaleFactor: textScale,
    ),
    Consumer<SecondsCN>(
      builder: (context, seconds, child) {
        return Text(
          seconds.string,
          textScaleFactor: textScale,
        );
      },
    ),
  ];
  if (!dropCentiseconds) {
    widgets.addAll([
      Text(
        ".",
        textScaleFactor: textScale,
      ),
      Consumer<CentisecondsCN>(
        builder: (context, centiseconds, child) {
          return Text(
            centiseconds.string,
            textScaleFactor: textScale,
          );
        },
      ),
    ]);
  }
  return SizedBox(
    width: 255,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: widgets,
    ),
  );
}

Widget deltaDisplay (double displayDelta, bool dropCentiseconds, double textScale, DeltaType deltaType) {
  List<Widget> widgets = [
    Text(
      "+",
      textScaleFactor: textScale,
    ),
    Consumer<MinutesCN>(
      builder: (context, minutes, child) {
        String text;
        switch (deltaType) {

          case DeltaType.touch:
            text = minutes.touchDeltaString;
            break;
          case DeltaType.start:
            text = minutes.startDeltaString;
            break;
        }
        return Text(
          text,
          textScaleFactor: textScale,
        );
      },
    ),
    Text(
      ":",
      textScaleFactor: textScale,
    ),
    Consumer<SecondsCN>(
      builder: (context, seconds, child) {
        String text;
        switch (deltaType) {

          case DeltaType.touch:
            text = seconds.touchDeltaString;
            break;
          case DeltaType.start:
            text = seconds.startDeltaString;
            break;
        }
        return Text(
          text,
          textScaleFactor: textScale,
        );
      },
    ),
  ];
  if (!dropCentiseconds) {
    widgets.addAll([
      Text(
        ".",
        textScaleFactor: textScale,
      ),
      Consumer<CentisecondsCN>(
        builder: (context, centiseconds, child) {
          String text;
          switch (deltaType) {

            case DeltaType.touch:
              text = centiseconds.touchDeltaString;
              break;
            case DeltaType.start:
              text = centiseconds.startDeltaString;
              break;
          }
          return Text(
            text,
            textScaleFactor: textScale,
          );
        },
      ),
    ]);
  }
  return Row(
    children: widgets,
  );
}