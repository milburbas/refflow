import 'package:flutter/material.dart';
import 'package:refflow/refflow_icons.dart';

import '../generated/l10n.dart';

Widget quickActionBar (
    void Function() onResetPressed,
    void Function() onSwapPressed,
    void Function() onIntermissionPressed,
    void Function() onPriorityPressed
    ) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      IconButton(
        icon: const Icon(
          RefflowIcons.rotate_left
        ),
        tooltip: S.current.resetBout,
        onPressed: onResetPressed
      ),
      IconButton(
          icon: const Icon(RefflowIcons.compare_arrows),
          tooltip: S.current.swapSides,
          onPressed: onSwapPressed
      ),
      IconButton(
          icon: const Icon(RefflowIcons.watch_later),
          tooltip: S.current.startIntermission,
          onPressed: onIntermissionPressed
      ),
      IconButton(
          icon: const Icon(RefflowIcons.stars),
          tooltip: S.current.assignPriority,
          onPressed: onPriorityPressed
      )
    ],
  );
}
