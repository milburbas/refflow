import 'package:flutter/cupertino.dart';

Widget scoreDisplay (
    int score,
    String name,
    bool priority
    ) {
  return SizedBox(
    width: 100,
    child: Column(
      children: <Widget>[
        nameDisplay(name, priority),
        scoreNumDisplay(score),
      ]
    ),
  );
}

Widget nameDisplay (String name, bool priority) {
  return Builder(
    builder: (context) {
      TextStyle style = DefaultTextStyle.of(context).style;
      Color? color = style.color;
      if (priority) {
        color = const Color.fromRGBO(224, 206, 0, 1);
      }
      return Text(name, style: TextStyle(color: color, fontSize: 20),);
    },
  );

}

Widget scoreNumDisplay (int score) {
  return Text(score.toString(), textScaleFactor: 3);
}