import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:refflow/util/lists.dart';

import '../data/bout/Cards.dart';
import '../generated/l10n.dart';
import '../util/enum.dart';

Widget cardDisplay(
  List<BoutCard> cards
) {
  List<Row> rows = List.empty(growable: true);
  List<List<BoutCard>> subsets = cards.sublist(0, min(cards.length, 5)).chunk(5);
  for (List<BoutCard> subset in subsets) {
    List<Widget> children = List.empty(growable: true);
    for (BoutCard card in subset) {
      Color color;
      switch(card.type!) {
        case CardType.yellow:
          color = const Color.fromARGB(255, 255, 255, 0);
          break;
        case CardType.red:
          color = const Color.fromARGB(255, 255, 0, 0);
          break;
        case CardType.black:
          color = const Color.fromARGB(255, 0, 0, 0);
          break;
      }
      Widget? child;
      if (card.P) {
        child = Center(child: Text(S.current.pCardShort, style: const TextStyle(color: Colors.black, fontSize: 11),));
      }
      children.add(
        Padding(
          padding: const EdgeInsets.all(0.5),
          child: Container(
            decoration: BoxDecoration(
              color: color,
              borderRadius: const BorderRadius.all(Radius.circular(32))
            ),
            height: 12,
            width: 12,
            child: child,
          ),
        )
      );
    }
    rows.add(
      Row(
        children: children,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
      )
    );
  }
  return SizedBox(
    height: 13,
    child: Column(
      children: rows,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
    ),
  );
}