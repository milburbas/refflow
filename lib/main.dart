import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:refflow/bout_info_screen.dart';
import 'package:refflow/bout_screen.dart';
import 'package:refflow/data/bout/Bout.dart';
import 'package:refflow/data/bout/model.dart';
import 'package:refflow/new_bout_screen.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'data/bout/Format.dart';
import 'generated/l10n.dart';


class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    String? route;
    Map? queryParameters;
    if (settings.name != null) {
      var uriData = Uri.parse(settings.name!);
      route = uriData.path;
      queryParameters = uriData.queryParameters;
    }
    return MaterialPageRoute(
      builder: (context) {
        switch(route) {
          case '/bout':
            BoutScreenArguments arguments = (settings.arguments ?? BoutScreenArguments(defaultBout.toString())) as BoutScreenArguments;
            return BoutScreen(bout: Bout.fromJson(json.decode(arguments.boutString)));
          case '/new':
            NewBoutScreenArguments arguments = settings.arguments as NewBoutScreenArguments;
            BoutFormat boutFormat;
            if (arguments.formatString != null) {
              boutFormat = BoutFormat.fromJson(json.decode(arguments.formatString!));
            } else {
              boutFormat = defaultBout.format;
            }
            return NewBoutScreen(boutFormat: boutFormat);
          case '/info':
            BoutInfoScreenArguments? arguments = settings.arguments as BoutInfoScreenArguments?;


            String? id;

            if (queryParameters?.containsKey('id') ?? false) {
              id = queryParameters!['id'];
            } else {
              id = arguments?.id;
            }

            Bout? bout;
            if (arguments?.boutString != null) {
              bout = Bout.fromJson(json.decode(arguments!.boutString!));
            } else {
              bout = null;
            }
            return BoutInfoScreen(bout: bout, id: id);
          default:
            return BoutScreen(bout: defaultBout);
        }
      },
      settings: settings,
    );
  }
}

void main() {
  runApp(
      const MyApp()
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Refref',
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
      theme: ThemeData(
        colorScheme: const ColorScheme(
            onSecondary: Colors.black,
            brightness: Brightness.dark,
            onError: Colors.grey,
            onBackground: Colors.white,
            error: Colors.pink,
            surface: Colors.blueGrey,
            primary: Colors.amber,
            secondary: Colors.amberAccent,
            onPrimary: Colors.black,
            background: Colors.black,
            onSurface: Colors.black
        )
      ),
      // initialRoute: '/bout',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

