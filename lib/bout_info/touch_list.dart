import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:refflow/data/bout/Phases.dart';
import 'package:refflow/util/lists.dart';
import 'package:refflow/util/sec_double_to_string.dart';

import '../data/bout/Bout.dart';
import '../data/bout/Score.dart';
import '../util/enum.dart';

Widget touchList(
  List<BoutScoreTouch> touches,
  double boutTime
) {
  List<Widget> entries = [];
  for (BoutScoreTouch touch in touches) {
    entries.add(touchEntry(touch, touches, boutTime));
  }
  return Column(
    children: entries,
  );
}

Widget touchEntry(
  BoutScoreTouch touch,
  List<BoutScoreTouch> touches,
  double boutTime
) {

  double touchBeforeTime = touches.lastWhereOrNull((element) => element.time < touch.time)?.time ?? 0;
  double touchAfterTime = touches.firstWhereOrNull((element) => element.time <= touch.time)?.time ?? boutTime;

  List<Widget> scoreIndicators = [];
  if ((touch.scorer ?? Fencer.left) == Fencer.left) {
    scoreIndicators.add(Container(
      decoration: const BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.all(Radius.circular(32))
        ),
        height: 16,
        width: 16
    ));
  } else {
    scoreIndicators.add(Container(
        decoration: const BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(32))
        ),
      height: 16,
      width: 16
    ));
  }
  scoreIndicators.add(
    const Padding(
      padding: EdgeInsets.all(8.0),
      child: Text("-"),
    )
  );
  if ((touch.scorer ?? Fencer.right) == Fencer.right) {
    scoreIndicators.add(Container(
        decoration: const BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.all(Radius.circular(32))
        ),
        height: 16,
        width: 16
    ));
  } else {
    scoreIndicators.add(Container(
        decoration: const BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(32))
        ),
        height: 16,
        width: 16
    ));
  }

  return Row(
    mainAxisSize: MainAxisSize.max,
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text("+${secDoubleToString(touch.time - touchBeforeTime, false)}"),
      Row(
        children: scoreIndicators
      ),
      Text("-${secDoubleToString(touchAfterTime - touch.time, false)}"),
    ],
  );
}