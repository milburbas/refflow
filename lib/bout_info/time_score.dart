import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:refflow/bout/notice_bar.dart';
import 'package:refflow/bout/quick_action_bar.dart';
import 'package:refflow/bout/score_display.dart';
import 'package:refflow/bout/time_row.dart';
import 'package:refflow/refflow_icons.dart';
import 'package:refflow/util/enum.dart';

import '../generated/l10n.dart';



Widget timeAndScore (
    void Function() onNewBoutPressed,
    String id,
    int touchGoal,
    int period,
    int? periods,
    double displayTime,
    double displayDelta,
    DeltaType deltaType,
    bool dropCentiseconds,
    int scoreLeft,
    int scoreRight,
    String? nameLeft,
    String? nameRight,
    Fencer? priority,
    Fencer? winner,
    bool isReversed,
    List<String> messages
    ) {
  List<Widget> children = [
    scoreDisplay(scoreLeft, nameLeft ?? S.current.leftNameDefault, priority == Fencer.left),
    Column (
      children: <Widget>[
        Row(
          children: <Widget>[
            touchGoalDisplay(touchGoal)
          ],
        )
      ],
    ),
    scoreDisplay(scoreRight, nameRight ?? S.current.rightNameDefault, priority == Fencer.right),
  ];
  if (isReversed) {
    children = children.reversed.toList();
  }

  return Column (
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Row(
        children: [
          Expanded(child: noticeBar(messages)),
          Builder(
            builder: (context) {
              List<PopupMenuEntry> items = [
                PopupMenuItem(
                  value: 1,
                  onTap: onNewBoutPressed,
                  child: Text(S.of(context).newBout),
                ),
              ];
              if (id != '') {
                PopupMenuItem(
                  value: 2,
                  child: Text(id, style: const TextStyle(color: Colors.grey)),
                  onTap: () {
                    Clipboard.setData(ClipboardData(text: id));
                  },
                );
              }
              return SizedBox(
                height: 36,
                child: PopupMenuButton(
                  itemBuilder: (context) => items,
                  icon: const Icon(RefflowIcons.menu),
                ),
              );
            }
          )
        ],
      ),
      timeRow(period = period, periods = periods, displayTime = displayTime, displayDelta = displayDelta, deltaType = deltaType, dropCentiseconds = dropCentiseconds),
      Row (
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: children
      ),
    ],
  );
}

Widget touchGoalDisplay (int? touchGoal) {
  String text = touchGoal != null ? S.current.touchGoal(touchGoal) : S.current.freefencing;
  return Text(text, textAlign: TextAlign.center,);
}