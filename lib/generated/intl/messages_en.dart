// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(period, periods) => "Period:\n${period} of ${periods}";

  static String m1(period) => "Period:\n${period}";

  static String m2(touchGoal) => "${touchGoal}\nTouch";

  static String m3(fencer) => "${Intl.select(fencer, {
            'left': 'Left Wins',
            'right': 'Right Wins',
            'other': 'No Winner',
          })}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "appTitle": MessageLookupByLibrary.simpleMessage("RefRef"),
        "assignPriority":
            MessageLookupByLibrary.simpleMessage("Assign Priority"),
        "breakMessage": MessageLookupByLibrary.simpleMessage("Break"),
        "clearPriority": MessageLookupByLibrary.simpleMessage("Clear Priority"),
        "doubleTouch": MessageLookupByLibrary.simpleMessage("Double"),
        "freeFencing": MessageLookupByLibrary.simpleMessage("Free Fencing"),
        "freefencing": MessageLookupByLibrary.simpleMessage("Free\nFencing"),
        "holdToUndo": MessageLookupByLibrary.simpleMessage("Hold\nTo\nUndo"),
        "intermission": MessageLookupByLibrary.simpleMessage("Intermission"),
        "leftNameDefault": MessageLookupByLibrary.simpleMessage("Left"),
        "manualEntry":
            MessageLookupByLibrary.simpleMessage("Manually enter a value."),
        "moreOptions": MessageLookupByLibrary.simpleMessage("More Options >"),
        "newBout": MessageLookupByLibrary.simpleMessage("New Bout"),
        "nextPeriod": MessageLookupByLibrary.simpleMessage("Next Period"),
        "noncombativity":
            MessageLookupByLibrary.simpleMessage("Non-Combativity"),
        "overtime": MessageLookupByLibrary.simpleMessage("Overtime"),
        "overtimeLength":
            MessageLookupByLibrary.simpleMessage("Overtime Length"),
        "pCardShort": MessageLookupByLibrary.simpleMessage("P"),
        "periodLength": MessageLookupByLibrary.simpleMessage("Period Length"),
        "periodOfPeriods": m0,
        "periodPeriod": m1,
        "periods": MessageLookupByLibrary.simpleMessage("Periods"),
        "pointLeft": MessageLookupByLibrary.simpleMessage("Point\nLeft"),
        "pointRight": MessageLookupByLibrary.simpleMessage("Point\nRight"),
        "priority": MessageLookupByLibrary.simpleMessage("Priority"),
        "priorityLeft": MessageLookupByLibrary.simpleMessage("Priority Left"),
        "priorityRight": MessageLookupByLibrary.simpleMessage("Priority Right"),
        "redCardShort": MessageLookupByLibrary.simpleMessage("R"),
        "resetBout": MessageLookupByLibrary.simpleMessage("Reset Bout"),
        "rightNameDefault": MessageLookupByLibrary.simpleMessage("Right"),
        "startAction": MessageLookupByLibrary.simpleMessage("Start"),
        "startIntermission":
            MessageLookupByLibrary.simpleMessage("Start Intermission"),
        "startTimer": MessageLookupByLibrary.simpleMessage("Start Timer"),
        "swapSides": MessageLookupByLibrary.simpleMessage("Swap Sides"),
        "timePerBreak": MessageLookupByLibrary.simpleMessage("Time per Break"),
        "touchGoal": m2,
        "touchesToWin": MessageLookupByLibrary.simpleMessage("Touches to Win"),
        "undoTouch": MessageLookupByLibrary.simpleMessage("Undo Touch"),
        "winMessage": m3,
        "yellowCardShort": MessageLookupByLibrary.simpleMessage("Y")
      };
}
