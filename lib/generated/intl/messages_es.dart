// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  static String m0(period, periods) => "Período:\n${period} de ${periods}";

  static String m1(period) => "Período:\n${period}";

  static String m2(touchGoal) => "${touchGoal}\nToques";

  static String m3(fencer) => "${Intl.select(fencer, {
            'left': 'Izquierdo Gana',
            'right': 'Derecho Gana',
            'other': 'Nadie Gana',
          })}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "appTitle": MessageLookupByLibrary.simpleMessage("RefRef"),
        "assignPriority": MessageLookupByLibrary.simpleMessage("Dar Prioridad"),
        "breakMessage": MessageLookupByLibrary.simpleMessage("Pausa"),
        "clearPriority":
            MessageLookupByLibrary.simpleMessage("Quitar Prioridad"),
        "doubleTouch": MessageLookupByLibrary.simpleMessage("Doble"),
        "freeFencing": MessageLookupByLibrary.simpleMessage("Combate Libre"),
        "freefencing": MessageLookupByLibrary.simpleMessage("Combate\nLibre"),
        "holdToUndo": MessageLookupByLibrary.simpleMessage(
            "Mantener Pulsado\nPara\nRevertir"),
        "intermission": MessageLookupByLibrary.simpleMessage("Tregua"),
        "leftNameDefault": MessageLookupByLibrary.simpleMessage("Izquierdo"),
        "manualEntry":
            MessageLookupByLibrary.simpleMessage("Introducir un Valor."),
        "moreOptions": MessageLookupByLibrary.simpleMessage("Más Opciones >"),
        "newBout": MessageLookupByLibrary.simpleMessage("Combate Nuevo"),
        "nextPeriod": MessageLookupByLibrary.simpleMessage("Período Siguiente"),
        "noncombativity":
            MessageLookupByLibrary.simpleMessage("No-Combatividad"),
        "overtime": MessageLookupByLibrary.simpleMessage("Tiempo Extra"),
        "overtimeLength":
            MessageLookupByLibrary.simpleMessage("Duración del Tiempo Extra"),
        "pCardShort": MessageLookupByLibrary.simpleMessage("P"),
        "periodLength":
            MessageLookupByLibrary.simpleMessage("Duración del Periodo"),
        "periodOfPeriods": m0,
        "periodPeriod": m1,
        "periods": MessageLookupByLibrary.simpleMessage("Cantidad de Períodos"),
        "pointLeft": MessageLookupByLibrary.simpleMessage("Toque\nIzquierdo"),
        "pointRight": MessageLookupByLibrary.simpleMessage("Toque\nDerecho"),
        "priority": MessageLookupByLibrary.simpleMessage("Prioridad"),
        "priorityLeft":
            MessageLookupByLibrary.simpleMessage("Prioridad al Izquierdo"),
        "priorityRight":
            MessageLookupByLibrary.simpleMessage("Prioridad al Derecho"),
        "redCardShort": MessageLookupByLibrary.simpleMessage("R"),
        "resetBout": MessageLookupByLibrary.simpleMessage("Reiniciar Combate"),
        "rightNameDefault": MessageLookupByLibrary.simpleMessage("Derecho"),
        "startAction": MessageLookupByLibrary.simpleMessage("Empezar"),
        "startIntermission":
            MessageLookupByLibrary.simpleMessage("Empezar Tregua"),
        "startTimer":
            MessageLookupByLibrary.simpleMessage("Empezar Cronómetro"),
        "swapSides": MessageLookupByLibrary.simpleMessage("Cambiar de Lado"),
        "timePerBreak":
            MessageLookupByLibrary.simpleMessage("Duración de la Pausa"),
        "touchGoal": m2,
        "touchesToWin":
            MessageLookupByLibrary.simpleMessage("Toques para Ganar"),
        "undoTouch": MessageLookupByLibrary.simpleMessage("Revertir Toque"),
        "winMessage": m3,
        "yellowCardShort": MessageLookupByLibrary.simpleMessage("A")
      };
}
