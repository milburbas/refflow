// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `RefRef`
  String get appTitle {
    return Intl.message(
      'RefRef',
      name: 'appTitle',
      desc: '',
      args: [],
    );
  }

  /// `Left`
  String get leftNameDefault {
    return Intl.message(
      'Left',
      name: 'leftNameDefault',
      desc: '',
      args: [],
    );
  }

  /// `Right`
  String get rightNameDefault {
    return Intl.message(
      'Right',
      name: 'rightNameDefault',
      desc: '',
      args: [],
    );
  }

  /// `Undo Touch`
  String get undoTouch {
    return Intl.message(
      'Undo Touch',
      name: 'undoTouch',
      desc: '',
      args: [],
    );
  }

  /// `Reset Bout`
  String get resetBout {
    return Intl.message(
      'Reset Bout',
      name: 'resetBout',
      desc: '',
      args: [],
    );
  }

  /// `Next Period`
  String get nextPeriod {
    return Intl.message(
      'Next Period',
      name: 'nextPeriod',
      desc: '',
      args: [],
    );
  }

  /// `Priority`
  String get priority {
    return Intl.message(
      'Priority',
      name: 'priority',
      desc: '',
      args: [],
    );
  }

  /// `Clear Priority`
  String get clearPriority {
    return Intl.message(
      'Clear Priority',
      name: 'clearPriority',
      desc: '',
      args: [],
    );
  }

  /// `New Bout`
  String get newBout {
    return Intl.message(
      'New Bout',
      name: 'newBout',
      desc: '',
      args: [],
    );
  }

  /// `Free Fencing`
  String get freeFencing {
    return Intl.message(
      'Free Fencing',
      name: 'freeFencing',
      desc: '',
      args: [],
    );
  }

  /// `{touchGoal}\nTouch`
  String touchGoal(Object touchGoal) {
    return Intl.message(
      '$touchGoal\nTouch',
      name: 'touchGoal',
      desc: '',
      args: [touchGoal],
    );
  }

  /// `Free\nFencing`
  String get freefencing {
    return Intl.message(
      'Free\nFencing',
      name: 'freefencing',
      desc: '',
      args: [],
    );
  }

  /// `Swap Sides`
  String get swapSides {
    return Intl.message(
      'Swap Sides',
      name: 'swapSides',
      desc: '',
      args: [],
    );
  }

  /// `Start Intermission`
  String get startIntermission {
    return Intl.message(
      'Start Intermission',
      name: 'startIntermission',
      desc: '',
      args: [],
    );
  }

  /// `Assign Priority`
  String get assignPriority {
    return Intl.message(
      'Assign Priority',
      name: 'assignPriority',
      desc: '',
      args: [],
    );
  }

  /// `Point\nLeft`
  String get pointLeft {
    return Intl.message(
      'Point\nLeft',
      name: 'pointLeft',
      desc: '',
      args: [],
    );
  }

  /// `Hold\nTo\nUndo`
  String get holdToUndo {
    return Intl.message(
      'Hold\nTo\nUndo',
      name: 'holdToUndo',
      desc: '',
      args: [],
    );
  }

  /// `Start Timer`
  String get startTimer {
    return Intl.message(
      'Start Timer',
      name: 'startTimer',
      desc: '',
      args: [],
    );
  }

  /// `Point\nRight`
  String get pointRight {
    return Intl.message(
      'Point\nRight',
      name: 'pointRight',
      desc: '',
      args: [],
    );
  }

  /// `Double`
  String get doubleTouch {
    return Intl.message(
      'Double',
      name: 'doubleTouch',
      desc: '',
      args: [],
    );
  }

  /// `Period:\n{period} of {periods}`
  String periodOfPeriods(Object period, Object periods) {
    return Intl.message(
      'Period:\n$period of $periods',
      name: 'periodOfPeriods',
      desc: '',
      args: [period, periods],
    );
  }

  /// `Period:\n{period}`
  String periodPeriod(Object period) {
    return Intl.message(
      'Period:\n$period',
      name: 'periodPeriod',
      desc: '',
      args: [period],
    );
  }

  /// `{fencer, select, left {Left Wins} right {Right Wins} other {No Winner}}`
  String winMessage(Object fencer) {
    return Intl.select(
      fencer,
      {
        'left': 'Left Wins',
        'right': 'Right Wins',
        'other': 'No Winner',
      },
      name: 'winMessage',
      desc: '',
      args: [fencer],
    );
  }

  /// `Break`
  String get breakMessage {
    return Intl.message(
      'Break',
      name: 'breakMessage',
      desc: '',
      args: [],
    );
  }

  /// `Overtime`
  String get overtime {
    return Intl.message(
      'Overtime',
      name: 'overtime',
      desc: '',
      args: [],
    );
  }

  /// `Intermission`
  String get intermission {
    return Intl.message(
      'Intermission',
      name: 'intermission',
      desc: '',
      args: [],
    );
  }

  /// `Priority Left`
  String get priorityLeft {
    return Intl.message(
      'Priority Left',
      name: 'priorityLeft',
      desc: '',
      args: [],
    );
  }

  /// `Priority Right`
  String get priorityRight {
    return Intl.message(
      'Priority Right',
      name: 'priorityRight',
      desc: '',
      args: [],
    );
  }

  /// `Period Length`
  String get periodLength {
    return Intl.message(
      'Period Length',
      name: 'periodLength',
      desc: '',
      args: [],
    );
  }

  /// `Overtime Length`
  String get overtimeLength {
    return Intl.message(
      'Overtime Length',
      name: 'overtimeLength',
      desc: '',
      args: [],
    );
  }

  /// `Touches to Win`
  String get touchesToWin {
    return Intl.message(
      'Touches to Win',
      name: 'touchesToWin',
      desc: '',
      args: [],
    );
  }

  /// `Periods`
  String get periods {
    return Intl.message(
      'Periods',
      name: 'periods',
      desc: '',
      args: [],
    );
  }

  /// `Time per Break`
  String get timePerBreak {
    return Intl.message(
      'Time per Break',
      name: 'timePerBreak',
      desc: '',
      args: [],
    );
  }

  /// `Start`
  String get startAction {
    return Intl.message(
      'Start',
      name: 'startAction',
      desc: '',
      args: [],
    );
  }

  /// `Manually enter a value.`
  String get manualEntry {
    return Intl.message(
      'Manually enter a value.',
      name: 'manualEntry',
      desc: '',
      args: [],
    );
  }

  /// `More Options >`
  String get moreOptions {
    return Intl.message(
      'More Options >',
      name: 'moreOptions',
      desc: '',
      args: [],
    );
  }

  /// `Non-Combativity`
  String get noncombativity {
    return Intl.message(
      'Non-Combativity',
      name: 'noncombativity',
      desc: '',
      args: [],
    );
  }

  /// `Y`
  String get yellowCardShort {
    return Intl.message(
      'Y',
      name: 'yellowCardShort',
      desc: '',
      args: [],
    );
  }

  /// `R`
  String get redCardShort {
    return Intl.message(
      'R',
      name: 'redCardShort',
      desc: '',
      args: [],
    );
  }

  /// `P`
  String get pCardShort {
    return Intl.message(
      'P',
      name: 'pCardShort',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
